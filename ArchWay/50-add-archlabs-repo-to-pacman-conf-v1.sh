#!/bin/bash
#set -e
##################################################################################################################
# Author	:	Erik Dubois
# Website	:	https://www.erikdubois.be
# Website	:	https://www.arcolinux.info
# Website	:	https://www.arcolinux.com
# Website	:	https://www.arcolinuxd.com
# Website	:	https://www.arcolinuxforum.com
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

echo '[archlabs_repo]
Server = https://bitbucket.org/archlabslinux/archlabs_repo/raw/master/$arch
Server = https://sourceforge.net/project/archlabs-repo/files/archlabs_repo/$arch' | sudo tee --append /etc/pacman.conf

echo "################################################################"
echo "###                  archlabs repo added                   ####"
echo "################################################################"
